Source: libdata-pond-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               libparams-classify-perl <!nocheck>,
               libtest-pod-coverage-perl <!nocheck>,
               libtest-pod-perl <!nocheck>,
               perl-xs-dev,
               perl:native
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdata-pond-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdata-pond-perl.git
Homepage: https://metacpan.org/release/Data-Pond
Rules-Requires-Root: no

Package: libdata-pond-perl
Architecture: any
Depends: ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
         libparams-classify-perl
Description: Perl-based open notation for data module
 Data::Pond is concerned with representing data structures in a textual
 notation known as "Pond" (Perl-based open notation for data). The notation is
 a strict subset of Perl expression syntax, but is intended to have
 language-independent use. It is similar in spirit to JSON, which is based on
 JavaScript, but Pond represents fewer data types directly.
 .
 The data that can be represented in Pond consist of strings (of characters),
 arrays, and string-keyed hashes. Arrays and hashes can recursively (but not
 cyclically) contain any of these kinds of data. This does not cover the full
 range of data types that Perl or other languages can handle, but is intended
 to be a limited, fixed repertoire of data types that many languages can
 readily process. It is intended that more complex data can be represented
 using these basic types. The arrays and hashes provide structuring facilities
 (ordered and unordered collections, respectively), and strings are a
 convenient way to represent atomic data.

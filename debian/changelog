libdata-pond-perl (0.005-3) unstable; urgency=medium

  * Add patch to use uvchr_to_utf8_flags instead of uvuni_to_utf8_flags.
    (Closes: #1065997)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Mar 2024 23:42:07 +0100

libdata-pond-perl (0.005-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 12 Jun 2022 23:11:34 +0100

libdata-pond-perl (0.005-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Damyan Ivanov ]
  * New upstream version 0.005
  * update years of upstream copyright
  * enable bindnow hardening option
  * declare conformance with Policy 4.1.1

 -- Damyan Ivanov <dmn@debian.org>  Fri, 03 Nov 2017 11:27:59 +0000

libdata-pond-perl (0.004-2) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Add explicit build dependency on libmodule-build-perl
  * Declare the package autopkgtestable

 -- Niko Tyni <ntyni@debian.org>  Fri, 05 Jun 2015 09:13:00 +0300

libdata-pond-perl (0.004-1) unstable; urgency=low

  * Initial release (closes: #719145).

 -- gregor herrmann <gregoa@debian.org>  Thu, 08 Aug 2013 21:19:14 +0200
